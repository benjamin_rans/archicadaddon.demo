// *****************************************************************************
// File:			Classification_Test.cpp
// Description:		Classification_Test add-on test functions
// Project:			APITools/Classification_Test
// Namespace:		-
// Contact person:	CSAT
// *****************************************************************************

// --- Includes ----------------------------------------------------------------
#include "APICommon.h"

#include "StringConversion.hpp"
#include	"APIEnvir.h"
#include	"ACAPinc.h"
#include	"DGModule.hpp"


// -----------------------------------------------------------------------------
// Add-on entry point definition
// -----------------------------------------------------------------------------
GSErrCode __ACENV_CALL APIMenuCommandProc_Main (const API_MenuParams *menuParams)
{
	if (menuParams->menuItemRef.menuResID == 32500) {
		return ACAPI_CallUndoableCommand ("Classification Test API Function",
			[&] () -> GSErrCode {
				try {
					switch (menuParams->menuItemRef.itemIndex) {
					case  1: WriteReport_Alert("Hello World!"); break;
					default: break;
					}
					return NoError;
				} catch (const GS::Exception&) {
					return Error;
				}
			}
		);
	}

	return NoError;
}		// APIMenuCommandProc_Main


// -----------------------------------------------------------------------------
// Dependency definitions
// -----------------------------------------------------------------------------

API_AddonType	__ACENV_CALL	CheckEnvironment (API_EnvirParams* envir)
{
	RSGetIndString (&envir->addOnInfo.name, 32000, 1, ACAPI_GetOwnResModule ());
	RSGetIndString (&envir->addOnInfo.description, 32000, 2, ACAPI_GetOwnResModule ());

	return APIAddon_Normal;
}


// -----------------------------------------------------------------------------
// Interface definitions
// -----------------------------------------------------------------------------

GSErrCode	__ACENV_CALL	RegisterInterface (void)
{
	return ACAPI_Register_Menu (32500, 32600, MenuCode_UserDef, MenuFlag_Default);
}


// -----------------------------------------------------------------------------
// Called after the Add-On has been loaded into memory
// -----------------------------------------------------------------------------

GSErrCode	__ACENV_CALL Initialize	(void)
{
	GSErrCode err = ACAPI_Install_MenuHandler (32500, APIMenuCommandProc_Main);

	return err;
}


// -----------------------------------------------------------------------------
// Called when the Add-On is going to be unloaded
// -----------------------------------------------------------------------------

GSErrCode	__ACENV_CALL FreeData	(void)
{
	return NoError;
}
