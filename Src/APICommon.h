﻿// *****************************************************************************
// Helper functions for Add-On development
// *****************************************************************************

#ifndef	_APICOMMON_H_
#define	_APICOMMON_H_

#include "GSRoot.hpp"
#include "UniString.hpp"

#if PRAGMA_ENUM_ALWAYSINT
	#pragma enumsalwaysint on
#elif PRAGMA_ENUM_OPTIONS
	#pragma options enum=int
#endif

/* -- Messages ------------------------------- */

void CCALL	WriteReport_Alert (const char* format, ...);

#if PRAGMA_ENUM_ALWAYSINT
	#pragma enumsalwaysint reset
#elif PRAGMA_ENUM_OPTIONS
	#pragma options enum=reset
#endif

#endif
