﻿// *****************************************************************************
// Helper functions for Add-On development
// *****************************************************************************

#include "APIEnvir.h"
#define	_APICOMMON_TRANSL_


// ---------------------------------- Includes ---------------------------------

#include	<stdio.h>
#include	<stdarg.h>
#include	<math.h>

#include	"GSSystem.h"

#include	"ACAPinc.h"
#include	"APICommon.h"


#define USE_DEBUG_WINDOW	1


// -----------------------------------------------------------------------------
// Write formatted info into the report window
// Give an alert also (with the same content)
// -----------------------------------------------------------------------------

void CCALL	WriteReport_Alert (const char* format, ...)
{
	va_list		argList;

	va_start (argList, format);
	ACAPI_WriteReport (format, true, argList);
	va_end (argList);

	return;
}		// WriteReport_Alert
